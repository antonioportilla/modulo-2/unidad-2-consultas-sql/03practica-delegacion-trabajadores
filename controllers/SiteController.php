<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    // se crea un controlador para una conslta numero 12
    public function actionConsulta12()
    {
            $dataProvider=new ActiveDataProvider([
            'query'=> Trabajadores::find(),
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
        return $this->render("list",[
            "resultados"=>$dataProvider,
            "campos"=>['id', 'nombre', 'fechanacimiento', 'foto', 'delegacion'],
            "titulo"=>"Consulta 12",
            "enunciado"=>"datos de trabajadores",
            "sql"=>"SELECT * FROM trabajadores",
        ]);
    }
    
        
    public function actionConsulta13()
    {
        
        return $this->render("vardump",[
            "datos"=>Trabajadores::find()->all(),
        ]); // abre la vista de siste vardump.php
               
    }
    
   
    
    public function actionConsulta15()
    {
     $dataProvider=new ActiveDataProvider([
            'query'=> Delegacion::find()->all(),
         ]);
         
         return $this->render("vardump",[
            "datos"=>$dataProvider,
         
        ]);
         
    }

    public function actionConsulta14()
    {
        // mediante Active Record
            $dataProvider=new ActiveDataProvider([
            'query'=> Delegacion::find(),
            'pagination'=>[
                'pageSize'=>20,
                ]
        ]);
                
        return $this->render("list",[
            "resultados"=>$dataProvider,
            "campos"=>['id', 'nombre', 'poblacion', 'direccion'],
            "titulo"=>"Consulta 14",
            "enunciado"=>"datos de delegaciones",
            "sql"=>"SELECT * FROM delegacion",
        ]);
    }
    
  
    public function actionConsulta14a(){
            
       // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(*) FROM delegacion" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT *  FROM delegacion" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>30,
                ],
        ]);
        
        return $this->render("list",[
            "resultados"=>$dataProvider,
            "campos"=>['id', 'nombre', 'poblacion', 'direccion'],
            "titulo"=>"Consulta 14a",
            "enunciado"=>"Lista la tabla delegaciones",
            "sql"=>"SELECT * FROM delegacion",
        ]);
    }
    

    
    
    
}
